- job-template:
    name: test_Tails_ISO_{name}
    parameters:
      - string:
          name: UPSTREAMJOB_BUILD_NUMBER
          description: "Not honored: https://gitlab.torproject.org/tpo/tpa/tails-sysadmin/-/issues/18133"
      - string:
          name: EXTRA_CUCUMBER_ARGS
          description: "Extra args passed to cucumber. (e.g. features/torified_browsing.feature)"
          default: ' '
      - string:
          name: EXTRA_SUITE_ARGS
          description: "Extra args passed to the test suite. (e.g. --disable-chutney)"
          default: ' '
    node: isoworker1.dragon || isoworker2.dragon || isoworker3.dragon || isoworker4.dragon || isoworker5.dragon || isoworker6.iguana || isoworker7.iguana || isoworker8.iguana
    builders:
      - shell: 'touch /tmp/test_Tails_ISO_job_started'
      - shell: 'mkdir tmp/'
      - shell: 'echo "Downloading the artifacts of upstream build job..."'
      - copyartifact:
          project: build_Tails_ISO_{name}
          filter: 'build-artifacts/tails-*.list,build-artifacts/tails-*.build-manifest,build-artifacts/tails-*.iso,build-artifacts/tails-*.img'
          target: tmp/
          flatten: true
          which-build: upstream-build
          fallback-to-last-successful: true
          do-not-fingerprint: true
      - inject:
          properties-file: tmp/tails-build-env.list
      - shell: 'USE_LAST_RELEASE_AS_OLD_ISO=yes /usr/local/bin/wrap_test_suite https://iso-history.tails.boum.org /tmp/TailsToaster $EXTRA_SUITE_ARGS -- $EXTRA_CUCUMBER_ARGS'
    concurrent: true
    description: '<p>This job is managed by jenkins-job-builder. Do not edit it through
      this web interface, your changes would be overwritten</p>'
    properties:
      - priority-sorter:
          priority: '2'
      - build-discarder:
          days-to-keep: 200
          artifact-days-to-keep: 200
    publishers:
    - postbuildscript:
        builders:
        - role: SLAVE
          build-on:
            - SUCCESS
            - UNSTABLE
            - FAILURE
            - ABORTED
          build-steps:
            # This high-priority job ensures that no other job is scheduled on
            # this node before reboot_node had time to do its job:
            - trigger-builds:
                - project: keep_node_busy_during_cleanup
                  same-node: true
            # This script will report test results back to any outstanding merge
            # requests in gitlab:
            - shell: '/usr/local/bin/talkback.py'
            # keep_node_busy_during_cleanup has highest priority so it will win
            # the race against other candidate jobs that are already queued and
            # would like to use this node if, and only if, these two conditions
            # are met:
            #
            #  - It is allowed to compete at all ⇔ it is already in the queue
            #    when the current test_Tails_ISO_{name} job finishes and frees
            #    this node. But it can take longer for
            #    keep_node_busy_during_cleanup to reach the queue than it takes
            #    the current job to complete.
            #
            #  - The intended priority has been assigned to it ⇔ the Priority
            #    Sorter plugin had time, after the keep_node_busy_during_cleanup
            #    build has been added to the queue, to sort it and move it to
            #    the top of the queue. This does not happen immediately.
            #
            # So before we put the node offline and reboot it, we need to give
            # Jenkins some time to instantiate the keep_node_busy_during_cleanup
            # job, add it to the queue, and move it to the top of the queue:
            - shell: 'sleep 30'
            # Chown + move artifacts we wants to archive to build-artifacts,
            # then delete others:
            - post_test_cleanup
    - archive:
        artifacts: build-artifacts/**
        latest-only: false
    - cucumber-reports:
        json-reports-path: build-artifacts
        file-include-pattern: cucumber.json
    - email:
        recipients: '{test_email}'
    - trigger-parameterized-builds:
      - project: reboot_node
        predefined-parameters: RESTART_NODE=${NODE_NAME}
    scm:
    - git:
        branches:
        - '{branch_name}'
        browser: gitlab
        browser-url: https://gitlab.tails.boum.org/tails/tails
        submodule:
          recursive: true
        skip-tag: true
        url: '{tails_git_remote}'
    wrappers:
    - timeout:
        type: "no-activity"
        timeout: 60
        timeout-var: 'BUILD_TIMEOUT'
        fail: true
        write-description: "Job run aborted: nothing happened since 60 minutes."
    - timestamps
